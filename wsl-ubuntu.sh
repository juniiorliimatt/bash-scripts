#!/bin/bash

update() {
  echo "Atualizando Distribuição"
  sudo apt update && sudo apt upgrade -y
}

zsh() {
  echo "Instalando ZSH"
  sudo apt install zsh
}

git() {
  sudo add-apt-repository ppa:git-core/ppa &&
  sudo apt update -y &&
  sudo apt install git -y &&
}

gitflow() {
  sudo apt install git-flow -y
}

node() {
  curl -fsSL https://deb.nodesource.com/setup_lts.x | sudo -E bash -
  sudo apt-get install -y nodejs
}

curl() {
  sudo apt install curl -y
}



update &&
zsh &&
git &&
gitflow &&
node &&
curl &&

